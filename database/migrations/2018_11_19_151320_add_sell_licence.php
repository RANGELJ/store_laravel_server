<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;

class AddSellLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Licence::create(["name" => "SELL_PRODUCTS"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Licence::where("name", "SELL_PRODUCTS")->forceDelete();
    }
}
