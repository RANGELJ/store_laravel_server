<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;

class AddPurchaseProductsLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $purchaceLicence = Licence::create(["name" => "PURCHASE_PRODUCTS"]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Licence::where(["name" => "PURCHASE_PRODUCTS"])->forceDelete();
    }
}
