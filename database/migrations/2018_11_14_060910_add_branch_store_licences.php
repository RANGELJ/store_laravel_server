<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;

class AddBranchStoreLicences extends Migration {

    private $licenceNames = [
        "ADD_BRANCH_STORES",
        "GET_OWN_BRANCH_STORES",
        "GET_ALL_BRANCH_STORES",
        "DELETE_OWN_BRANCH_STORES",
        "DELETE_ALL_BRANCH_STORES",
        "UPDATE_OWN_BRANCH_STORES",
        "UPDATE_ALL_BRANCH_STORES",
        "RESTORE_BRANCH_STORES",
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        foreach ($this->licenceNames as $licenceName) {
            Licence::create(["name" => $licenceName]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        foreach ($this->licenceNames as $licenceName) {
            Licence::where(["name" => $licenceName])->first()->forceDelete();
        }
    }
}
