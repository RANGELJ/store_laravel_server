<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        User::create([
            "name" => "JRANGELP",
            "email" => "developer@devtools.com",
            "password" => bcrypt("13280041"),
        ]);

        User::create([
            "name" => "ADMINE",
            "email" => "admin@devtools.com",
            "password" => bcrypt("13280041"),
        ]);

        User::create([
            "name" => "STOCKMAN",
            "email" => "stockmanager@devtools.com",
            "password" => bcrypt("13280041"),
        ]);

        User::create([
            "name" => "STOCKOPP",
            "email" => "stockoperator@devtools.com",
            "password" => bcrypt("13280041"),
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
