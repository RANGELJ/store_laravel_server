<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Role;
use App\Licence;

class AddRestoreProductLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $adminRole = Role::where("name","ADMIN")->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $restoreLicence = Licence::create(["name" => "RESTORE_PRODUCTS"]);
        
        $adminRole->licences()->attach($restoreLicence);
        $stockManagerRole->licences()->attach($restoreLicence);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $adminRole = Role::where("name","ADMIN")->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $restoreLicence = Licence::where("name", "RESTORE_PRODUCTS")->first();
        $adminRole->licences()->detach($restoreLicence);
        $stockManagerRole->licences()->detach($restoreLicence);
        $restoreLicence->delete();
    }
}
