<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Licence;
use App\Models\Role;
use App\Models\Development;

class CreateLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('licences', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 45)->unique();
            $table->timestamps();
        });

        Schema::create("licence_user", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("licence_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();

            $table->foreign("licence_id")->references("id")->on("licences");
            $table->foreign("user_id")->references("id")->on("users");
        });

        Schema::create("licence_role", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("licence_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->timestamps();

            $table->foreign("licence_id")->references("id")->on("licences");
            $table->foreign("user_id")->references("id")->on("users");
        });

        $adminRole = Role::where("name", "ADMIN")->first();

        $stockManagerRole = Role::create(["name" => "STOCK_MANAGER"]);
        $stockOperatorRole = Role::create(["name" => "STOCK_OPERATOR"]);

        $addProductLicence = Licence::create([
            "name" => "ADD_PRODUCTS",
        ]);

        $adminRole->licences()->attach($addProductLicence->id);
        $stockManagerRole->licences()->attach($addProductLicence);

        Development::getStockManagerUser()->roles()->attach($stockManagerRole);
        Development::getStockOperatorUser()->roles()->attach($stockOperatorRole);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("licence_role");
        Schema::dropIfExists("licence_user");
        Schema::dropIfExists('licences');
    }
}
