<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id")->unsigned();
            $table->integer("inventory_id")->unsigned();
            $table->integer("type_id")->unsigned();
            $table->integer("quantity");
            $table->integer("before");
            $table->integer("after");
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("inventory_id")->references("id")->on("inventories");
            $table->foreign("type_id")->references("id")->on("inventory_movement_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_movements');
    }
}
