<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;
use App\Models\Role;

class AddGetLicences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $getOwnProductLicence = Licence::create(["name" => "GET_OWN_PRODUCTS"]);
        $getAllProductLicence = Licence::create(["name" => "GET_ALL_PRODUCTS"]);

        $adminRole = Role::where(["name" => "ADMIN"])->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $storeOperatorRole = Role::where("name", "STOCK_OPERATOR")->first();
        $adminRole->licences()->attach($getAllProductLicence);
        $stockManagerRole->licences()->attach($getAllProductLicence);
        $storeOperatorRole->licences()->attach($getAllProductLicence);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $getOwnProductLicence = Licence::where(["name" => "GET_OWN_PRODUCTS"])->first();
        $getAllProductLicence = Licence::where(["name" => "GET_ALL_PRODUCTS"])->first();
        $adminRole = Role::where(["name" => "ADMIN"])->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $storeOperatorRole = Role::where("name", "STOCK_OPERATOR")->first();
        $adminRole->licences()->detach($getAllProductLicence->id);
        $stockManagerRole->licences()->detach($getAllProductLicence);
        $storeOperatorRole->licences()->detach($getAllProductLicence);
        $getAllProductLicence->delete();
        $getOwnProductLicence->delete();
    }
}
