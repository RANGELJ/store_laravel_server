<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\InventoryMovementType;

class CreateInventoryMovementTypesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('inventory_movement_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 45)->unique();
            $table->boolean("isIn");
            $table->timestamps();
        });

        InventoryMovementType::create([
            "name" => "Purchase",
            "isIn" => true,
        ]);

        InventoryMovementType::create([
            "name" => "Sell",
            "isIn" => false,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('inventory_movement_types');
    }

}
