<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Role;
use App\User;
use App\Models\Development;

class AddBasicRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $adminRole = Role::create([
            "name" => "ADMIN",
        ]);

        Development::getAdminUser()->roles()->attach($adminRole);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $adminRole = Role::where("name", "ADMIN")->first();
        Development::getAdminUser()->roles()->detach($adminRole->id);
        $adminRole->delete();
    }
}
