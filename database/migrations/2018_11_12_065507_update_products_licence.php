<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Role;
use App\Licence;

class UpdateProductsLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $updateOwnProductsLicence = Licence::create(["name" => "UPDATE_OWN_PRODUCTS"]);
        $updateAllProductsLicence = Licence::create(["name" => "UPDATE_ALL_PRODUCTS"]);
        $adminRole = Role::where("name","ADMIN")->first();
        $adminRole->licences()->attach($updateAllProductsLicence);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $updateOwnProductsLicence = Licence::where("name","UPDATE_OWN_PRODUCTS")->first();
        $updateAllProductsLicence = Licence::where("name", "UPDATE_ALL_PRODUCTS")->first();
        $adminRole = Role::where("name","ADMIN")->first();
        $adminRole->licences()->detach($updateAllProductsLicence->id);
        $updateAllProductsLicence->delete();
        $updateOwnProductsLicence->delete();
    }
}
