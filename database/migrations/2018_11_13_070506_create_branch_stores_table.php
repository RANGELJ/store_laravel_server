<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('branch_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 45)->unique();
            $table->integer("user_adds")->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("user_adds")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('branch_stores');
    }
}
