<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Licence;
use App\Models\Development;

class CreateWarehousesTable extends Migration {

    private $licences = [
        "GET_WAREHOUSES",
        "ADD_WAREHOUSE",
        "DELETE_WAREHOUSE",
        "UPDATE_WAREHOUSE",
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name", 45);
            $table->integer("branchstore_id")->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("branchstore_id")->references("id")->on("branch_stores");
        });

        foreach($this->licences as $licenceName) {
            Licence::create(["name" => $licenceName]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        foreach ($this->licences as $licenceName) {
            Licence::where(["name" => $licenceName])->forceDelete();
        }
        Schema::dropIfExists('warehouses');
    }
}
