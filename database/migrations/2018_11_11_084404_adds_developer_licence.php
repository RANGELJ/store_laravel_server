<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;
use App\User;
use App\Models\Development;

class AddsDeveloperLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $developerLicence = Licence::create([
            "name" => "IS_DEVELOPER",
        ]);

        Development::getDevUser()->licences()->attach($developerLicence->id);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $developerLicence = Licence::where(["name" => "IS_DEVELOPER"])->first();
        $devUser = Development::getDevUser();
        $devUser->licences()->detach($developerLicence->id);
        $developerLicence->delete();
    }
}
