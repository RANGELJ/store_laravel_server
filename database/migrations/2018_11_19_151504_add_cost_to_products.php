<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table("products", function(Blueprint $table) {
            $table->decimal("average_purchase_price", 20, 4)
                ->default(0.0)->unsigned()->after("name");
        });

        Schema::table("inventory_movements", function(Blueprint $table) {
            $table->decimal("unitary_price", 20, 4)->after("after");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table("products", function(Blueprint $table) {
            $table->dropColumn("average_purchase_price");
        });
        Schema::table("inventory_movements", function(Blueprint $table) {
            $table->dropColumn("unitary_price");
        });
    }
}
