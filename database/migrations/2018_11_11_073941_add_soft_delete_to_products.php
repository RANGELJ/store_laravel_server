<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Licence;
use App\Models\Role;

class AddSoftDeleteToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table("products", function(Blueprint $table) {
            $table->softDeletes();
        });

        $deleteOwnProductsLicence = Licence::create([
            "name" => "DELETE_OWN_PRODUCTS",
        ]);

        $deleteProductsLicence = Licence::create([
            "name" => "DELETE_PRODUCTS",
        ]);

        $adminRole = Role::where("name", "ADMIN")->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();

        $adminRole->licences()->attach($deleteOwnProductsLicence->id);
        $adminRole->licences()->attach($deleteProductsLicence->id);
        $stockManagerRole->licences()->attach($deleteOwnProductsLicence);
        $stockManagerRole->licences()->attach($deleteProductsLicence);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $adminRole = Role::where("name", "ADMIN")->first();
        $deleteOwnProductsLicence = Licence::
            where("name", "DELETE_OWN_PRODUCTS")->first();
        $deleteProductsLicence = Licence::
            where("name", "DELETE_PRODUCTS")->first();
        $adminRole->licences()->detach($deleteOwnProductsLicence->id);
        $adminRole->licences()->detach($deleteProductsLicence->id);
        $stockManagerRole->licences()->detach($deleteOwnProductsLicence);
        $stockManagerRole->licences()->detach($deleteProductsLicence);

        $deleteOwnProductsLicence->delete();
        $deleteProductsLicence->delete();

        Schema::table("products", function(Blueprint $table) {
            $table->dropColumn("deleted_at");
        });
    }
}
