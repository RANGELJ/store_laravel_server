<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Licence;

class AddGetWarehousesStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Licence::create([
            "name" => "GET_WAREHOUSES_STOCK",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Licence::where("name", "GET_WAREHOUSES_STOCK")->forceDelete();
    }
}
