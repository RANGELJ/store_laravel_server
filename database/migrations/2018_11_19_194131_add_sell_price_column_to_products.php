<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellPriceColumnToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table("products", function(Blueprint $table) {
            $table->decimal("sell_price", 40, 4)->default(0.0)->unsigned()
                ->after("average_purchase_price");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table("products", function(Blueprint $table) {
            $table->dropColumn("sell_price");
        });
    }
}
