<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Role;
use App\Licence;

class AddGetDeletedProductsLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        $getDeletedProducts = Licence::create(["name" => "GET_DELETED_PRODUCTS"]);
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $adminRole = Role::where(["name" => "ADMIN"])->first();
        $stockManagerRole->licences()->attach($getDeletedProducts);
        $adminRole->licences()->attach($getDeletedProducts);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        $getDeletedProducts = Licence::where("name", "GET_DELETED_PRODUCTS")->first();
        $adminRole = Role::where(["name" => "ADMIN"])->first();
        $stockManagerRole = Role::where("name", "STOCK_MANAGER")->first();
        $adminRole->licences()->detach($getDeletedProducts);
        $stockManagerRole->licences()->detach($getDeletedProducts);
        $getDeletedProducts->delete();
    }
}
