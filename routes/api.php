<?php

use Illuminate\Http\Request;

Route::post('user/register', 'APIAuthController@register');
Route::post('user/login', 'APIAuthController@login');

Route::middleware('jwt.auth')->group( function () {

    Route::post("application/development/cleandatabase", "Development\DeveloperToolsController@cleanDatabase");

    Route::post("admin/branchstores/add", "Admin\BranchStoreController@add");
    Route::get("admin/branchstores/get", "Admin\BranchStoreController@get");

    Route::get("stock/products/get", "Stock\ProductsController@get");
    Route::post("stock/products/add", "Stock\ProductsController@add");
    Route::post("stock/products/delete", "Stock\ProductsController@delete");
    Route::get("stock/products/deleted/get", "Stock\ProductsController@getDeleted");
    Route::post("stock/products/deleted/resore", "Stock\ProductsController@restore");
    Route::post("stock/products/update", "Stock\ProductsController@update");

    Route::post("stock/warehouses/add", "Stock\WarehousesController@add");

    Route::get("stock/warehouses/inventory/stock", "Stock\WarehousesController@stock");
    Route::post("stock/warehouses/inventory/purchase", "Stock\WarehousesController@purchase");
    Route::post("stock/warehouses/inventory/sell", "Stock\WarehousesController@sell");

});
