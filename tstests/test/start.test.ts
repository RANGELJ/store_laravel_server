import LaravelClient from "./models/LaravelClient";
import { DEV_USER_TOKEN, setDevUserToken, setAdminUserToken, setStockManagerToken, setStockOperatorToken } from "./testcofig";
import ProductTester from "./testslist/ProductTester";
import BranchstoresTester from "./testslist/BranchstoresTester";
import ClientUser from "./models/ClientUser";
import WarehouseTester from "./testslist/WarehousesTester";

const laravelClient = new LaravelClient();
const productTester = new ProductTester();
const branchstoresTester = new BranchstoresTester();

beforeAll(async () => {
    console.clear();
    setDevUserToken(await laravelClient.logUser("developer@devtools.com", "13280041"));
    setAdminUserToken(await laravelClient.logUser("admin@devtools.com", "13280041"));
    setStockManagerToken(await laravelClient.logUser("stockmanager@devtools.com", "13280041"));
    setStockOperatorToken(await laravelClient.logUser("stockoperator@devtools.com", "13280041"));
});

beforeEach(async () => {
    let response = await laravelClient.post("application/development/cleandatabase", {
        headers: {
            "Authorization": "Bearer " + DEV_USER_TOKEN,
        },
    });
    laravelClient.isASuccessfullRequest(response);
});

test("Products", async () => {
    await productTester.test();
});

test("Bransh Stores", async () => {
    await branchstoresTester.test();
});

test("Warehouses", async () => {
    await WarehouseTester.test();
});

test("Inventory management", async () => {

    const devUser = new ClientUser(DEV_USER_TOKEN);

    let branchStore: {id: number, name: string} = { id: undefined, name: "Branch 1" };

    const branchStoreData = await devUser.post("admin/branchstores/add", {body: {
        name: branchStore.name,
    }});
    devUser.isASuccessfullRequest(branchStoreData);
    branchStore.id = branchStoreData.data.id;

    let warehouses: {id: number, name: string}[] = [
        { id: undefined, name: "Ware1" },
        { id: undefined, name: "Ware2" },
        { id: undefined, name: "Ware3" },
    ];

    warehouses = await Promise.all(warehouses.map(async (warehouse) => {
        const warehouseData = await devUser.post("stock/warehouses/add", {body: {
            branchStoreId: branchStore.id,
            name: warehouse.name,
        }});
        devUser.isASuccessfullRequest(warehouseData);
        warehouse.id = warehouseData.data.id;

        return warehouse;
    }));

    let products: Array<{id: number, name: string}> = [
        { id: undefined, name: "Baby's Socks" },
        { id: undefined, name: "Baby food" },
    ];

    products = await Promise.all(products.map(async (product) => {
        const productData = await devUser.post("stock/products/add", { body: {
            name: product.name,
        } });
        devUser.isASuccessfullRequest(productData);
        product.id = productData.data.newProduct.id;
        return product;
    }));

    const purchases = [
        { productIndex: 1, warehouseIndex: 0, price: 12.3, quantity: 5, before: 0, after: 5 },
        { productIndex: 1, warehouseIndex: 0, price: 20, quantity: 20, before: 5, after: 25 },
        { productIndex: 1, warehouseIndex: 1, price: 19.2, quantity: 60, before: 0, after: 60 },
        { productIndex: 0, warehouseIndex: 0, price: 15, quantity: 4, before: 0, after: 4 },
        { productIndex: 1, warehouseIndex: 0, price: 30.1, quantity: 5, before: 25, after: 30 },
    ];

    let response;

    for (let purchase of purchases) {

        const warehouse = warehouses[purchase.warehouseIndex];
        const product = products[purchase.productIndex];

        response = await devUser.get("stock/warehouses/inventory/stock", { body: {
            warehouse_id: warehouse.id,
            product_id: product.id,
        } });
        devUser.isASuccessfullRequest(response);
        expect(response.data).toBe(purchase.before);

        response = await devUser.post("stock/warehouses/inventory/purchase", { body: {
            warehouse_id: warehouse.id,
            product_id: product.id,
            unitary_price: purchase.price,
            quantity: purchase.quantity
        } });
        devUser.isASuccessfullRequest(response);

        response = await devUser.get("stock/warehouses/inventory/stock", { body: {
            warehouse_id: warehouse.id,
            product_id: product.id,
        } });
        devUser.isASuccessfullRequest(response);
        expect(response.data).toBe(purchase.after);
    }

    response = await devUser.post("stock/products/update", { body: {
        product_id: products[0].id,
        sell_price: 18.10,
    } });
    devUser.isASuccessfullRequest(response);

    response = await devUser.post("stock/products/update", { body: {
        product_id: products[1].id,
        sell_price: 30,
    } });
    devUser.isASuccessfullRequest(response);

    response = await devUser.post("stock/warehouses/inventory/sell", { body: {
        warehouse_id: warehouses[0].id,
        product_id: products[1].id,
        quantity: 2
    } });
    devUser.isASuccessfullRequest(response);

});
