
export const BASE_URL = "http://localhost/storeapp/";

export const CLIENT_HASH = "UnFfgePOwSFN8Qgxitum0okojVHCiOs5eKZFIIhX";

export let DEV_USER_TOKEN = "";
export let ADMIN_USER_TOKEN = "";
export let STOCK_MANAGER_TOKEN = "";
export let STOCK_OPERATOR_USER = "";

export function setDevUserToken(token: string) {
    DEV_USER_TOKEN = token;
}

export function setAdminUserToken(token: string) {
    ADMIN_USER_TOKEN = token;
}

export function setStockManagerToken(token: string) {
    STOCK_MANAGER_TOKEN = token;
}

export function setStockOperatorToken(token: string) {
    STOCK_OPERATOR_USER = token;
}
