
import axios from "axios";
import { BASE_URL, CLIENT_HASH } from "../testcofig";

export type RequestPaylot = {body?: {}, headers?: {}};

export default class LaravelClient {

    public isUnauthenticatedRequest(response: any) {
        expect(response.message).toBe("Unauthenticated.");
    }

    public isValidatorError(response: any) {
        this.isAFailedRequest(response);
        expect(response.message).toBe("validation error");
    }

    public isASuccessfullRequest(response: any) {
        expect(response.errors).not.toBeDefined();
        expect(response.data).toBeDefined();
        expect(response.message).toBeDefined();
    }

    public isAFailedRequest(response: any) {
        expect(response.errors).toBeDefined();
        expect(response.data).not.toBeDefined();
        expect(response.message).toBeDefined();
    }

    public async logUser(email: string, password: string) {
        const response = await this.post("user/login", {
            body: {
                email,
                password,
            }
        });
        if (! response.data) {
            console.log(response);
        }
        this.isASuccessfullRequest(response);
        return response.data.token;
    }

    public async post(url: string, params: RequestPaylot = {body: {}, headers: {}}) {
        try {
            const response = await axios.post(BASE_URL + url, params.body, {
                headers: {
                    "Accept": "application/json",
                    ...params.headers
                }
            });
            return response.data;
        } catch (error) {
            return error.response.data;
        }
    }

    public async get(url: string, params: RequestPaylot = {body: {}, headers: {}}) {
        try {
            const response = await axios.get(BASE_URL + url, {
                params: params.body,
                headers: {
                    "Accept": "application/json",
                    ...params.headers
                }
            });
            return response.data;
        } catch (error) {
            return error.response.data;
        }
    }

}
