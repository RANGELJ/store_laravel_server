import LaravelClient, { RequestPaylot } from "./LaravelClient";

export default class ClientUser extends LaravelClient {

    private token: string;

    constructor(token: string) {
        super();
        this.token = token;
    }

    public async post(url: string, params: RequestPaylot = {body: {}, headers: {}}) {
        params.headers = {
            "Authorization": "Bearer " + this.token,
            ...params.headers,
        };
        return await super.post(url, params);
    }

    public async get(url: string, params: RequestPaylot = {body: {}, headers: {}}) {
        params.headers = {
            "Authorization": "Bearer " + this.token,
            ...params.headers,
        };
        return await super.get(url, params);
    }

}
