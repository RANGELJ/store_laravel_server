import ClientUser from "../models/ClientUser";
import { DEV_USER_TOKEN } from "../testcofig";
import LaravelClient from "../models/LaravelClient";

export default class BranchstoresTester {

    public async test() {
        const laravelClient = new LaravelClient();
        const devUser = new ClientUser(DEV_USER_TOKEN);
        const NEW_BRANCH = {
            name: "The great branch",
        };

        let response = await devUser.post("admin/branchstores/add", {
            body: {
                name: NEW_BRANCH.name,
            },
        });
        devUser.isASuccessfullRequest(response);
        const savedBranchStore = response.data;
        
        response = await devUser.get("admin/branchstores/get");
        expect(response.data.length).toBe(1);
        expect(response.data[0].id).toBe(savedBranchStore.id);
    }

}
