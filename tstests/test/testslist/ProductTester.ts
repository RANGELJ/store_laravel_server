import { DEV_USER_TOKEN } from "../testcofig";
import ClientUser from "../models/ClientUser";

export default class ProductTester {

    private devUser: ClientUser;

    public async test() {
        this.devUser = new ClientUser(DEV_USER_TOKEN);

        const NEW_PRODUCT_NAME = "Baby pacifier";

        let response = await this.devUser.post("stock/products/add");
        this.devUser.isAFailedRequest(response);
        expect(response.errors.name).toBeDefined();

        response = await this.devUser.post("stock/products/add", {
            body: {
                name: NEW_PRODUCT_NAME,
            },
        });
        this.devUser.isASuccessfullRequest(response);
        expect(response.message).toBe("Product registered");
        expect(response.data.newProduct).toBeDefined();

        const idNewProduct = response.data.newProduct.id;

        response = await this.devUser.get("stock/products/get");
        this.devUser.isASuccessfullRequest(response);
        const productInGet = response.data[0];
        expect(productInGet).toBeDefined();
        expect(productInGet.name).toBe(NEW_PRODUCT_NAME);

        response = await this.devUser.post("stock/products/update", {
            body: {
                product_id: productInGet.id,
                name: "MODIFIED PRODUCT"
            },
        });
        this.devUser.isASuccessfullRequest(response);
        expect(response.data.id).toBe(productInGet.id);

        response = await this.devUser.post("stock/products/update", {
            body: {
                product_id: productInGet.id,
                name: NEW_PRODUCT_NAME
            },
        });
        this.devUser.isASuccessfullRequest(response);
        expect(response.data.id).toBe(productInGet.id);
        expect(response.data.name).toBe(productInGet.name);

        response = await this.devUser.post("stock/products/delete");
        this.devUser.isAFailedRequest(response);
        expect(response.errors.productId).toBeDefined();

        response = await this.devUser.post("stock/products/delete", {
            body: {
                productId: idNewProduct,
            },
        });
        this.devUser.isASuccessfullRequest(response);

        response = await this.devUser.get("stock/products/get");
        this.devUser.isASuccessfullRequest(response);
        expect(response.data.length).toBe(0);

        response = await this.devUser.get("stock/products/deleted/get");
        this.devUser.isASuccessfullRequest(response);
        expect(response.data.length).toBe(1);

        response = await this.devUser.post("stock/products/deleted/resore", {
            body: {
                productId: productInGet,
            },
        });
        this.devUser.isASuccessfullRequest(response);

        response = await this.devUser.get("stock/products/get");
        this.devUser.isASuccessfullRequest(response);
        expect(response.data.length).toBe(1);
    }
}
