import ClientUser from "../models/ClientUser";
import { DEV_USER_TOKEN } from "../testcofig";

export default class WarehouseTester {

    public static async test(): Promise<void> {
        const devUser = new ClientUser(DEV_USER_TOKEN);

        let response = await devUser.post("admin/branchstores/add", {body: {
            name: "First branch"
        }});
        devUser.isASuccessfullRequest(response);
        const branch = response.data;

        response = await devUser.post("stock/warehouses/add", {body: {
            name: "warehouse", branchStoreId: branch.id,
        }});
        devUser.isASuccessfullRequest(response);
        const newWarehouse = response.data;
        expect(newWarehouse).toBeDefined();
    }

}
