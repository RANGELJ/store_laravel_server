<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    protected $fillable = ["name"];

    public function licences() {
        return $this->belongsToMany("App\Licence", "licence_role",
            "user_id", "licence_id")->withTimestamps();
    }

}
