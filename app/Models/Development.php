<?php

namespace App\Models;

use App\User;

class Development {

    public static function getDevUser() {
        return User::where("email","developer@devtools.com")->first();
    }

    public static function getAdminUser() {
        return User::where("email","admin@devtools.com")->first();
    }

    public static function getStockManagerUser() {
        return User::where("email","stockmanager@devtools.com")->first();
    }

    public static function getStockOperatorUser() {
        return User::where("email","stockoperator@devtools.com")->first();
    }

}
