<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', "email_verified_at",
            "updated_at", "created_at"
    ];

    public function hasLicence(string $licenceName) {

        $hasLicence = $this->roles->reduce(function($hasLicence, $role) use ($licenceName) {
            $validLicences = $role->licences->filter(function($licence) use ($licenceName) {
                return $licence->name === $licenceName;
            });

            return (count($validLicences) > 0) || $hasLicence;
        }, false);

        if ($hasLicence) {
            return $hasLicence;
        }

        $hasLicence = $this->licences->reduce(function($hasLicence, $licence) use ($licenceName) {
            return $hasLicence || $licence->name === $licenceName || $licence->name === "IS_DEVELOPER";
        }, false);

        return $hasLicence;
    }

    public function licences() {
        return $this->belongsToMany("App\Licence", "licence_user",
            "user_id", "licence_id")->withTimestamps();
    }

    public function roles() {
        return $this->belongsToMany('App\Models\Role', "role_user",
            'user_id', 'role_id')->withTimestamps();
    }

}
