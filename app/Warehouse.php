<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model {
    use SoftDeletes;

    protected $fillable = ["name", "branchstore_id"];

    protected $dates = ['deleted_at'];

    public function inventories() {
        return $this->hasMany("App\Inventory");
    }

}
