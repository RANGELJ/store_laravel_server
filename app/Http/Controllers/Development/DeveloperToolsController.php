<?php

namespace App\Http\Controllers\Development;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Product;
use App\Warehouse;
use App\Inventory;
use App\InventoryMovement;
use App\Models\Admin\BranchStore;

class DeveloperToolsController extends BaseController {

    public function cleanDatabase(Request $request) {

        if (! $request->user()->hasLicence("IS_DEVELOPER") ) {
            $this->sendUnauthorized();
        }

        DB::transaction(function() {
            InventoryMovement::where("id", ">", 0)->forceDelete();
            Inventory::where("id", ">", 0)->forceDelete();
            Product::where("id", ">", 0)->forceDelete();
            Warehouse::where("id", ">", 0)->forceDelete();
            BranchStore::where("id", ">", 0)->forceDelete();
        });

        return $this->sendSuccess([], "database cleaned");

    }

}
