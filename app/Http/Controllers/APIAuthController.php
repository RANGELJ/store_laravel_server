<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\BaseController as BaseController;

class APIAuthController extends BaseController {

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|min:5|max:20|unique:users',
            'password'=> 'required|confirmed'
        ]);
        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }
        User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);
        $user = User::first();
        $token = JWTAuth::fromUser($user);
        
        return $this->sendSuccess(compact('token'), "registration successfull");
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $credentials = $request->only('email', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->sendError("invalid credentials", [], 401);
            }
        } catch (JWTException $e) {
            return $this->sendError("could not create token", [], 500);
        }

        return $this->sendSuccess(compact('token'), "logged in");
    }

}
