<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\Models\Admin\BranchStore;

class BranchStoreController extends BaseController {

    public function get(Request $request) {
        $canGetOwnBranches = $request->user()->hasLicence("GET_OWN_BRANCH_STORES");
        $canGetAllBranches = $request->user()->hasLicence("GET_ALL_BRANCH_STORES");

        if (! $canGetOwnBranches && ! $canGetAllBranches) {
            return $this->sendUnauthorized();
        }

        $branchs = BranchStore::when(! $canGetAllBranches, function($query) use ($request) {
            return $query->where("user_adds", $request->user()->id );
        })->when($request->name, function($query, $name) {
            return $query->where("name", "like", "%$name%");
        })->get();

        return $this->sendSuccess($branchs, "branch fetched");
    }

    public function add(Request $request) {

        if (! $request->user()->hasLicence("ADD_BRANCH_STORES")) {
            $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "name" => "required|unique:branch_stores|max:45|min:4",
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $branchStore = BranchStore::create([
            "name" => $request->name,
            "user_adds" => $request->user()->id,
        ]);

        return $this->sendSuccess($branchStore, "branch store created");

    }

}
