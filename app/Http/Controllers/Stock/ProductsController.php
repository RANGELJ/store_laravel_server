<?php

namespace App\Http\Controllers\Stock;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\Product;

class ProductsController extends BaseController {

    public function add(Request $request) {

        if (! $request->user()->hasLicence("ADD_PRODUCTS")) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "name" => "required|unique:products|max:45|min:5",
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $newProduct = Product::create([
            "name" => $request->name,
            "user_adds" => $request->user()->id,
        ]);

        return $this->sendSuccess([
            "newProduct" => $newProduct,
        ], "Product registered");
    }

    public function delete(Request $request) {

        $validator = Validator::make($request->all(), [
            "productId" => "required|exists:products,id",
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $productToDelete = Product::find($request->productId);

        $isOwnProduct = $productToDelete->user_adds === $request->user()->id;
        $canDeleteOwnProducts = $request->user()->hasLicence("DELETE_OWN_PRODUCTS");
        $canDeleteAllProducts = $request->user()->hasLicence("DELETE_PRODUCTS");

        if ($isOwnProduct) {
            if (! ($canDeleteOwnProducts || $canDeleteAllProducts ) ) {
                return $this->sendUnauthorized();
            }
        } else if (! $canDeleteAllProducts) {
            return $this->sendUnauthorized();
        }

        if ($productToDelete->delete()) {
            return $this->sendSuccess([], "product deleted");
        } else {
            return $this->sendError("error deleting product");
        }
    }

    public function get(Request $request) {

        $canGetAllProducts = $request->user()->hasLicence("GET_ALL_PRODUCTS");
        $canGetOwnProducts = $request->user()->hasLicence("GET_OWN_PRODUCTS");

        $canOnlyGetOwnProducts = $canGetOwnProducts && ! $canGetAllProducts;

        if (! $canGetAllProducts && ! $canGetOwnProducts) {
            return $this->sendUnauthorized();
        }

        $products = Product::when($request->name, function($query, $name) {
            return $query->where("name", "like", "%$name%");
        })->when($canOnlyGetOwnProducts, function($query) use ($request) {
            return $query->where("user_adds", $request->user()->id);
        })->get();

        return $this->sendSuccess($products, "products fetched");
    }

    public function getDeleted(Request $request) {

        if (! $request->user()->hasLicence("GET_DELETED_PRODUCTS")) {
            return $this->sendUnauthorized();
        }

        $deletedProducts = Product::onlyTrashed()
        ->when($request->name, function($query, $name) {
            return $query->where("name", "like", "%$name%");
        })->get();

        return $this->sendSuccess($deletedProducts, "deleted products listed");
    }

    public function restore(Request $request) {

        if (! $request->user()->hasLicence("RESTORE_PRODUCTS")) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "productId" => "required"
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $product = Product::onlyTrashed()->where("id", $request->productId);

        $product->restore();

        return $this->sendSuccess([], "product restored");
    }

    public function update(Request $request) {
        $canUpdateAllProducts = $request->user()->hasLicence("UPDATE_ALL_PRODUCTS");
        $canUpdateOwnProducts = $request->user()->hasLicence("UPDATE_OWN_PRODUCTS");

        if (! $canUpdateAllProducts && ! $canUpdateOwnProducts) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "product_id" => "required|exists:products,id",
            "name" => "unique:products|max:45|min:5",
            "sell_price" => "regex:/[0-9]+(\\.[0-9]{1,4})?/",
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $productToUpdate = Product::find($request->product_id);

        if ($request->name) {
            $productToUpdate->name = $request->name;
        }

        if ($request->sell_price) {
            $productToUpdate->sell_price = $request->sell_price;
        }

        $productToUpdate->save();

        return $this->sendSuccess($productToUpdate, "product updated");
    }

}
