<?php

namespace App\Http\Controllers\Stock;

use App\Http\Controllers\BaseController as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;

use App\Warehouse;
use App\Product;
use App\InventoryMovementType;
use App\Inventory;
use App\InventoryMovement;
use App\Models\Admin\BranchStore;

class WarehousesController extends BaseController {

    public function add(Request $request) {

        if (! $request->user()->hasLicence("ADD_WAREHOUSE") ) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "name" => "required|max:45|min:5",
            "branchStoreId" => "required|exists:branch_stores,id"
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $branchDefined = Warehouse::where("id", $request->branchStoreId)
            ->where("name", $request->name)
            ->first();
        if ($branchDefined) {
            return $this->sendError("Warehouse already defined", []);
        }

        $newWarehouse = Warehouse::create([
            "name" => $request->name,
            "branchstore_id" => $request->branchStoreId,
        ]);

        return $this->sendSuccess($newWarehouse, "warehouse created");
    }

    public function stock(Request $request) {

        if (! $request->user()->hasLicence("GET_WAREHOUSES_STOCK")) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "warehouse_id" => "required|exists:warehouses,id",
            "product_id" => "required|exists:products,id"
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $inventory = Inventory::where(
            "warehouse_id", $request->warehouse_id
        )->where("product_id", $request->product_id)->first();

        $quantity = ($inventory) ? $inventory->quantity : 0;

        return $this->sendSuccess($quantity, "inventory quantity");
    }

    public function sell(Request $request) {

        if (! $request->user()->hasLicence("SELL_PRODUCTS") ) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "warehouse_id" => "required|exists:warehouses,id",
            "product_id" => "required|exists:products,id",
            "quantity" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $productToSell = Product::find($request->product_id);

        $sellMovementType = InventoryMovementType::where([
            "name" => "Sell",
        ])->first();

        $inventory = Inventory::firstOrCreate([
            "warehouse_id" => $request->warehouse_id,
            "product_id" => $request->product_id,
        ]);
        $inventory->refresh();

        DB::beginTransaction();
            $inventory->performMovement(
                $request->user()->id,
                $sellMovementType->id,
                $productToSell->sell_price,
                $request->quantity
            );
        DB::commit();

        return $this->sendSuccess([
            "quantity_after" => $inventory->quantity,
        ], "sell registered");

    }

    public function purchase(Request $request) {

        if (! $request->user()->hasLicence("PURCHASE_PRODUCTS")) {
            return $this->sendUnauthorized();
        }

        $validator = Validator::make($request->all(), [
            "warehouse_id" => "required|exists:warehouses,id",
            "product_id" => "required|exists:products,id",
            "unitary_price" => "required|regex:/[0-9]+(\\.[0-9]{1,4})?/",
            "quantity" => "required|numeric|min:1"
        ]);

        if ($validator->fails()) {
            return $this->sendErrorFromValidator($validator);
        }

        $purchaceMovementType = InventoryMovementType::where([
            "name" => "Purchase",
        ])->first();

        $inventory = Inventory::firstOrCreate([
            "warehouse_id" => $request->warehouse_id,
            "product_id" => $request->product_id,
        ]);
        $inventory->refresh();

        DB::beginTransaction();

        $inventoryPrice = $inventory->computeTotalPrice();
        $purchasePrice = $request->unitary_price * $request->quantity;
        $newInventoryPrice = $inventoryPrice + $purchasePrice;

        $inventory->performMovement(
            $request->user()->id,
            $purchaceMovementType->id,
            $request->unitary_price,
            $request->quantity
        );

        $inventory->product->average_purchase_price = $newInventoryPrice / $inventory->quantity;
        $inventory->product->save();

        DB::commit();

        return $this->sendSuccess([
            "quantity_after" => $inventory->quantity,
        ], "purchase registered");
    }

}
