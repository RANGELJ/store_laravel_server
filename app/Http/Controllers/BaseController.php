<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller {
    
    /**
     * When the controller action is successfull,
     * it includes a message with the brief of the action taken
     */
    public function sendSuccess($result, $message) {

        $response = [
            "data" => $result,
            "message" => $message,
        ];

        return response()->json($response, 200);
    }

    public function sendErrorFromValidator($validator) {
        return $this->sendError("validation error", $validator->errors());
    }

    /**
     * @param error The error main message
     * @param errorMessages An array with detailed errors
     * @param code Default 404, the code to be sended with the response
     */
    public function sendError($error, $errorMessages = [], $code = 404) {
    	$response = [
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['errors'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    public function sendUnauthorized() {
        $response = [
            "message" => "Unauthorized.",
        ];

        return response()->json($response, 401);
    }

}
