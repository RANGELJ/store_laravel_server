<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryMovementType extends Model {

    protected $fillable = ["name", "isIn"];

}
