<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryMovement extends Model {
    
    protected $fillable = [
        "user_id", "inventory_id", "unitary_price", "type_id", "quantity",
        "before", "after"
    ];

}
