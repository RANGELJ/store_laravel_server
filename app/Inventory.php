<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\InventoryMovementType;

class Inventory extends Model {
    
    protected $fillable = ["quantity", "warehouse_id", "product_id"];

    public function performMovement(
        int $userId,
        int $movementTypeId,
        $unitary_price,
        int $quantity
    ) {
        $movementType = InventoryMovementType::find($movementTypeId);

        $quantity = ($movementType->isIn) ? $quantity : -$quantity;

        $beforeMovement = $this->quantity;
        $afterMovement = $beforeMovement + $quantity;

        $movement = InventoryMovement::create([
            "user_id" => $userId,
            "inventory_id" => $this->id,
            "type_id" => $movementTypeId,
            "unitary_price" => $unitary_price,
            "quantity" => $quantity,
            "before" => $beforeMovement,
            "after" => $afterMovement,
        ]);

        $this->quantity = $afterMovement;
        $this->save();
    }

    public function computeTotalPrice() {
        $productPrice = $this->product->average_purchase_price;
        return $productPrice * $this->quantity;
    }

    public function product() {
        return $this->belongsTo("App\Product");
    }

}
